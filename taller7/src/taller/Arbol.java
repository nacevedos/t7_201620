package taller;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import taller.interfaz.IReconstructorArbol;

public class Arbol implements IReconstructorArbol
{
	private String[] inorden;
	private String[] preorden;
	private Nodo raiz;
	public class Nodo<T>
	{
		private Nodo left;
		private Nodo right;
		private int size = 0;
		private T value;
		
		public Nodo(T val)
		{
			this.value = val;
		}
	}
	public Arbol(String nombre) throws IOException
	{
		inorden = new String[9];
		preorden = new String[9];
		cargarArchivo(nombre);
	}
	@Override
	public void cargarArchivo(String nombre) throws IOException 
	{
		System.out.println("Archivo:");
		System.out.println(nombre);
		File archivo = new File(nombre);
		// TODO Auto-generated method stub
		Properties prop = new Properties();
		FileInputStream in = new FileInputStream("./data/"+archivo);
		prop.load(in);
		preorden = prop.getProperty("preorden").split(",");
		inorden = prop.getProperty("inorden").split(",");
		System.out.println(preorden.length);
		System.out.println(inorden.length);
		in.close(); 
	}
	@Override
	public void crearArchivo(String info) throws FileNotFoundException, UnsupportedEncodingException 
	{
		// TODO Auto-generated method stub


		JSONArray json = new JSONArray();

		JSONObject raiz1 = new JSONObject();
		try {
			raiz1.put("arbol", raiz);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		json.put(raiz1);

		Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls()
				.create();
		try {
			DataOutputStream dos = new DataOutputStream(new FileOutputStream(
					new File(info)));
			dos.writeBytes(gson.toJson(json).replace("\n", "\r\n"));
			dos.flush();
			dos.close();
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		 
	}

	@Override
	public void reconstruir()
	{
		// TODO Auto-generated method stub
		int preComienzo = 0;
		int preFin = preorden.length-1;
		int inComienzo = 0;
		int inFin = inorden.length-1;
		raiz = construct(preorden, preComienzo, preFin, inorden, inComienzo, inFin);
		
	}
	public Nodo<String> construct(String[] preorden, int preComienzo, int preFin, String[] inorden, int inComienzo, int inFin)
	{
		if(preComienzo>preFin||inComienzo>inFin)
		{
			return null;
		}
		String val = preorden[preComienzo];
		Nodo n = new Nodo<String>(val);
		int k=0;
		for(int i=0; i<inorden.length; i++)
		{
			if(val.equals(inorden[i]))
			{
				k=i;
				break;
			}
		}
		n.left = construct(preorden, preComienzo+1, preComienzo+(k-inComienzo), inorden, inComienzo, k-1);
		n.right = construct(preorden, preComienzo+(k-inComienzo+1), preFin, inorden, k+1 , inFin);
		return n;
	}
	public boolean contieneArbol(Nodo T)
	{
		
        if (raiz == null) 
            return true;
  
        if (T == null)
            return false;
  
        
        if (sonIguales(T, raiz)) 
            return true;
  
        return contieneArbol(T.left)
                || contieneArbol(T.right);
	}
	boolean sonIguales(Nodo raiz1, Nodo raiz2) 
    {
        if (raiz1 == null && raiz2 == null)
            return true;
  
        if (raiz1 == null || raiz2 == null)
            return false;
  
        /* Check if the data of both roots is same and data of left and right
           subtrees are also same */
        return (raiz1 == raiz2
                && sonIguales(raiz1.left, raiz2.left)
                && sonIguales(raiz1.right, raiz2.right));
    }
	public Object darRaiz() {
		// TODO Auto-generated method stub
		return raiz;
	}
}
