package taller.test;


import java.io.IOException;
import junit.framework.TestCase;
import taller.Arbol;
import taller.Arbol.Nodo;


public class ArbolTest extends TestCase{

	private Arbol arbol;

	private void setupEscenario1() {

		try {
			arbol = new Arbol("data/ejemplo2.properties");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void setupEscenario2() {

		try {
			arbol = new Arbol("data/ejemplo3.properties");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public void testConstruir() 
	{
		setupEscenario1();
		arbol.reconstruir();
		String s = arbol.darRaiz().toString();
		assertEquals(s, "a");
	}

	public void testRaiz() 
	{
		setupEscenario2();
		arbol.reconstruir();
		String s = arbol.darRaiz().toString();
		assertEquals(s, "j");
	}

	public void testSub() 
	{
		setupEscenario1();
		arbol.reconstruir();
		Nodo<String> no = arbol.new Nodo<String>("c");
		boolean si = arbol.contieneArbol(no);

		assertTrue(si);
	}

	public void testSub2() 
	{
		setupEscenario2();
		arbol.reconstruir();
		Nodo<String> no = arbol.new Nodo<String>("g");
		boolean si = arbol.contieneArbol(no);

		assertTrue(si);
	}

}