package taller.interfaz;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Scanner;

import taller.Arbol;

public class ArbolCLI {

	private Scanner in;

	public ArbolCLI()
	{
		in = new Scanner(System.in);
	}

	public void mainMenu()
	{
		boolean finish = false;
		while(!finish)
		{	
			Screen.clear();
			System.out.println("------------------------------------------");
			System.out.println("-                                        -");
			System.out.println("-           Siembra de árboles           -");
			System.out.println("-                                        -");
			System.out.println("------------------------------------------");
			System.out.println("EL sistema para la plantación de árboles binarios\n");

			System.out.println("Menú principal:");
			System.out.println("-----------------");
			System.out.println("1. Cargar archivo con semillas");
			System.out.println("2. Salir");
			System.out.print("\nSeleccione una opción: ");
			int opt1 = in.nextInt();
			switch(opt1)
			{
			case 1:
				try {
					recibirArchivo();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				finish = true;
				break;
			case 2:
				finish = true;
				break;
			default:
				break;
			}
		}
	}

	public void recibirArchivo() throws IOException
	{
		boolean finish = false;
		while(!finish)
		{
			Screen.clear();
			System.out.println("Recuerde que el archivo a cargar");
			System.out.println("debe ser un archivo properties");
			System.out.println("que tenga la propiedad in-orden,");
			System.out.println("la propiedad pre-orden (donde los ");
			System.out.println("elementos estén separados por comas) y");
			System.out.println("que esté guardado en la carpeta data.");
			System.out.println("");
			System.out.println("Introduzca el nombre del archivo:");
			System.out.println("----------------------------------------------------");
			String nombreArch=in.next(); 
			// TODO Leer el archivo .properties 
//			Properties p= new Properties();
//			File archivo=new File("/data/ejemplo.properties");
//			FileInputStream in1;
//			try {
//				in1 = new FileInputStream(archivo);
//				p.load(in1);
//				in1.close();
//			} catch (FileNotFoundException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} 
//			
//			String preOrder= p.getProperty("preorden");
//			String inOrder = p.getProperty("inOrder"); 
			Arbol a= new Arbol(nombreArch); 
			
			try {
				
				// TODO cargarArchivo
				a.cargarArchivo(nombreArch);
				// TODO Reconstruir árbol  
				a.reconstruir();
				
				System.out.println("Introduzca donde desea crear el arbol:");
                System.out.println("----------------------------------------------------");
            	nombreArch = in.next();
            	a.crearArchivo(nombreArch);

				System.out.println("Ha creado el arbol!\nPara verlo vaya a  /data/arbolPlantado.json");
				
				System.out.println("Presione 1 para salir");
				in.next();
				finish=true;

			} catch (Exception e) {
				System.out.println("Hubo un problema cargando el archivo:");
				System.out.println(e.getMessage());
				e.printStackTrace();

			}
		}
	}
} 